import React from "react";

import { Modal } from "antd";

function ModalComponent({
  modalWidth = 750,
  onCloseIconClick = () => {},
  children,
  childrenClassName,
  showFooter = true,
  isModalOpen = true,
  modalTitle = "Modal Title",
  closable = () => {},
}) {
  return (
    <>
      <Modal
        width={modalWidth}
        footer={null}
        className="newModelClass"
        closable={closable}
        open={isModalOpen}
        modalTitle={modalTitle}
        onOk={""}
        onCancel={onCloseIconClick}
      >
        <>
          <div
            className="d-flex justify-content-between px-3 py-4"
            // style={{fontFamily:900}}
          >
            <label
              className="fw-900 ff-open-sans"
              style={{
                fontWeight: "700",
                fontSize: "20px",
                marginTop: "-30px",
              }}
            >
              <p>{modalTitle}</p>
            </label>
          </div>
          <div className={childrenClassName}>{children}</div>
          {!showFooter && (
            <div style={{ borderTop: "1px solid #E5ECFC" }}></div>
          )}
        </>
      </Modal>
    </>
  );
}

export default ModalComponent;
