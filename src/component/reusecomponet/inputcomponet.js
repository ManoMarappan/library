import React from "react";

import { Input } from "antd";

function InputBoxComponent({
  className = "",
  placeholder = "",
  value = "",
  onChange = () => {},
  size = "default",
  style = {},
  status = "", //error or warning
  readOnly = false,
  type = "",
  name=""
}) {
  return (
    <div>
      <Input
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        size={size}
        style={style}
        status={status}
        readOnly={readOnly}
        type={type}
        name={name}
        className={className}
      />
    </div>
  );
}

export default InputBoxComponent;
