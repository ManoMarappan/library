import { useState } from "react";
import ModalComponent from "../reusecomponet/Modelcomponet";
import InputBoxComponent from "../reusecomponet/inputcomponet";
import { DatePicker } from "antd";
import dayjs from "dayjs";

const LibraryForm = ({ openmodel, onclickIcon, getdatafromModel }) => {
  const [errorimage, seterrorimage] = useState({});
  const [errortitle, seterrortitle] = useState({});
  const [errorAuthor, seterrorAuthor] = useState({});
  const [formData, setformData] = useState({
    CoverImage: "",
    launchdate: "",
    Title: "",
    Author: "",
  });
  // console.log("data", formData);
  const handlechange = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.value });
  };
  const onSubmit = () => {
    let validateimageurl = validimage(formData.CoverImage);
    console.log(validateimageurl);
    // let validatelaunchdate=validlaunchdate(userdata.launchdate)
    let validatetitle = validtitle(formData.Title);
    let validateAuthor = validAuthor(formData.Author);

    if (validateimageurl && validatetitle && validateAuthor) {
      getdatafromModel(formData);
      onclickIcon(false);
      setformData({
        CoverImage: "",
        launchdate: "",
        Title: "",
        Author: "",
      });
    }
  };
  const validimage = (image) => {
    if (image) {
      if (image.match(/\.(jpg|jpeg|png|gif)$/)) {
        seterrorimage({
          ...errorimage,
          CoverImage: " ",
        });
        return true;
      } else {
        seterrorimage({
          ...errorimage,
          CoverImage: "select not valid image url",
        });
      }
    } else {
      seterrorimage({
        ...errorimage,
        CoverImage: "image url requried",
      });
    }
  };
  const validtitle = (title) => {
    if (title) {
      if (title.match(/^[a-zA-Z ]{2,40}$/)) {
        seterrortitle({
          ...errortitle,
          Title: "",
        });
        return true;
      } else {
        seterrortitle({
          ...errortitle,
          Title: " not valid Title ",
        });
      }
    } else {
      seterrortitle({
        ...errortitle,
        Title: "Title requried",
      });
    }
  };
  let validAuthor = (author) => {
    if (author) {
      if (author.match(/^[a-zA-Z ]{2,40}$/)) {
        seterrorAuthor({
          ...errorAuthor,
          Author: "",
        });
        return true;
      } else {
        seterrorAuthor({
          ...errorAuthor,
          Author: "Author name is not valid",
        });
      }
    } else {
      seterrorAuthor({
        ...errorAuthor,
        Author: "Author required",
      });
      return false;
    }
  };

  return (
    <div>
      <ModalComponent isModalOpen={openmodel} onCloseIconClick={onclickIcon} modalTitle="Library">
        <label style={{fontWeight:600}}>Cover Iamge</label>
        <InputBoxComponent
          name="CoverImage"
          value={formData.CoverImage}
          onChange={(e) => {
            handlechange(e);
          }}
        />
        <span className="text-danger">{errorimage.CoverImage}</span>
        <br></br>
        <label  style={{fontWeight:600}}>launchdate</label>
        <DatePicker
          name="launchdate"
          value={dayjs(formData.launchdate, "YYYY-MM-DD")}
          onChange={(e, s) =>
            setformData({
              ...formData,
              launchdate: s,
            })
          }
        />
        <br></br>
        <label  style={{fontWeight:600}}>Title</label>
        <InputBoxComponent
          name="Title"
          value={formData.Title}
          onChange={(e) => {
            handlechange(e);
          }}
        />
        <span className="text-danger">{errortitle.Title}</span>
        <br></br>
        <label  style={{fontWeight:600}}>Author</label>
        <InputBoxComponent
          name="Author"
          value={formData.Author}
          onChange={(e) => {
            handlechange(e);
          }}
        />
          <span className="text-danger">{errorAuthor.Author}</span>
          <br></br>
        <button onClick={onSubmit}>Submit</button>
      </ModalComponent>
    </div>
  );
};
export default LibraryForm;
