import React, { useEffect, useState } from "react";
import "../maincomponent/maincomponent.css";
import ModalComponent from "../reusecomponet/Modelcomponet";
import Description from "../form/descriptionmodel";

const LibraryPage = () => {
  const [storeData, setStoreData] = useState([]);
  const [openmodel, setopenmodel] = useState(false);
  console.log(storeData);
  const getdatafromApi = () => {
    let url =
      "http://demo.api.admin.circlesnow.com/ProductRESTService.svc/getschedmsg";
    const email = "manomanu.2668@gmail.com";
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        token: email,
      },
    };

    fetch(url, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        const data = JSON.parse(result.dt);
        setStoreData(data);
      })
      .catch((error) => {});
  };
  useEffect(() => {
    getdatafromApi();
  }, []);

  const onclicknewblog = () => {
    setopenmodel(true);
    // postData();
  };
  const onclickIcon = () => {
    setopenmodel(false);
  };
  const postData = () => {
    const url =
      "http://demo.api.admin.circlesnow.com/ProductRESTService.svc/schedMsg";
    const email = "manomanu.2668@gmail.com";

    let data = {
      title: "book  my show ",
      launchdate: "01-may-2023",
      author: "mano mano",
      image_link:
        "https://images.pexels.com/photos/16124576/pexels-photo-16124576/free-photo-of-music-musician-piano-keyboard.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load",
      description: "mano",
    };
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        token: email,
      },
      body: JSON.stringify(data),
    };

    fetch(url, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  return (
    <div className="main_continar">
      <div className="header_continer">
        <div className="h1_Contianer">
          <p>Library</p>
        </div>
        <div>
          <button className="Btn_continer" onClick={onclicknewblog}>
            New blog
          </button>
        </div>
      </div>
      <div>
        <table class="table table-striped">
          <thead class="tablehead">
            <tr >
              <th>Cover Iamge</th>
              <th>launch date</th>
              <th > Title</th>
              <th>Author</th>
            </tr>
          </thead>
          <tbody>
            {storeData.map((values, index) => {
              return (
                <>
                  <tr>
                    <th scope="row">
                      <img src={values.image_lnk} width={50} height={50} />
                    </th>
                    <td>{values.launchdate.substring(0, 10)}</td>
                    <td onClick={onclicknewblog} style={{color:"#009BE9"}}>{values.title}</td>
                    <td>{values.author}</td>
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      </div>
      {openmodel ? (
        <Description openmodel={openmodel} onclickIcon={onclickIcon} />
      ) : (
        ""
      )}
    </div>
  );
};
export default LibraryPage;
