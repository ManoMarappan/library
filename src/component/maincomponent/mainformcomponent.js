import React, { useEffect, useState } from "react";
import "../maincomponent/maincomponent.css";
import ModalComponent from "../reusecomponet/Modelcomponet";
import Description from "../form/descriptionmodel";
import LibraryForm from "../form/formModel";

const LibraryFormPage = () => {
  const [storeData, setStoreData] = useState([]);
  const [storeData2, setStoreData2] = useState([]);
  console.log(storeData);

  const [openmodel, setopenmodel] = useState(false);
  // console.log(storeData);
  // const getdatafromApi = () => {
  //   let url =
  //     "http://demo.api.admin.circlesnow.com/ProductRESTService.svc/getschedmsg";
  //   const email = "manomanu.2668@gmail.com";
  //   const requestOptions = {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //       token: email,
  //     },
  //   };

  //   fetch(url, requestOptions)
  //     .then((response) => response.json())
  //     .then((result) => {
  //       const data = JSON.parse(result.dt);
  //       setStoreData(data);
  //     })
  //     .catch((error) => {});
  // };
  // useEffect(() => {
  //   getdatafromApi();
  // }, []);

  const onclicknewblog = () => {
    setopenmodel(true);
    // postData();
  };
  const onclickIcon = () => {
    setopenmodel(false);
  };
  // const postData = () => {
  //   const url =
  //     "http://demo.api.admin.circlesnow.com/ProductRESTService.svc/schedMsg";
  //   const email = "manomanu.2668@gmail.com";

  //   let data = {
  //     title: storeData2.Title,
  //     launchdate: storeData2.launchdate,
  //     author: storeData2.Author,
  //     image_link: storeData2.CoverImage,
  //     description: "fghjkl",
  //   };
  //   console.log(data.title);
  //   const requestOptions = {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //       token: email,
  //     },
  //     body: JSON.stringify(data),
  //   };

  //   fetch(url, requestOptions)
  //     .then((response) => response.json())
  //     .then((result) => {
  //       console.log(result);
  //     })
  //     .catch((error) => {
  //       console.error("Error:", error);
  //     });
  // };

  const getdatafromModel = (data) => {
    console.log("data", data);
    const copydata = [...storeData];
    copydata.push(data);
    setStoreData(copydata);
  };

  return (
    <div className="main_continar">
      <div className="header_continer">
        <div className="h1_Contianer">
          <p>Library</p>
        </div>
        <div>
          <button className="Btn_continer" onClick={onclicknewblog}>
            New blog
          </button>
        </div>
      </div>
      <div>
        <table class="table table-striped">
          <thead class="tablehead">
            <tr>
              <th>Cover Iamge</th>
              <th>launch date</th>
              <th> Title</th>
              <th>Author</th>
            </tr>
          </thead>
          <tbody>
            {storeData.map((values, index) => {
              return (
                <>
                  <tr>
                    <th scope="row">
                      <img src={values.image_lnk} width={50} height={50} />
                    </th>
                    <td>{values.launchdate.substring(0, 10)}</td>
                    <td style={{color:" #009BE9"}} >{values.Title}</td>
                    <td>{values.Author}</td>
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      </div>
      <LibraryForm
        openmodel={openmodel}
        onclickIcon={onclickIcon}
        getdatafromModel={getdatafromModel}
      />
    </div>
  );
};
export default LibraryFormPage;
